/* global L, distance */

var pot;

// seznam z markerji na mapi
var markerji = [];

var mapa;
var obmocje;
var rezultati = [];

const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;


/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */
window.addEventListener('load', function () {

  // Osnovne lastnosti mape
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 12
    // maxZoom: 3
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

  // Ročno dodamo fakulteto za računalništvo in informatiko na mapo
  dodajMarker(FRI_LAT, FRI_LNG, "FAKULTETA ZA RAČUNALNIŠTVO IN INFORMATIKO", "FRI");


  // Objekt oblačka markerja
  var popup = L.popup();

  function obKlikuNaMapo(e) {
    console.log("Funkcija ob kliku na mapo");
    var latlng = e.latlng;
    // pridobimo koordinate klika 
    /*
        popup
          .setLatLng(latlng)
          .setContent("Izbrana bolnisnica: ")
          .openOn(mapa);

        prikazPoti(latlng);
    */    
        
    // klicemo funkcijo za izris pobaevanih bolnisnic in popupa
    // posredujemo koordinate
    pridobiPodatke(function(jsonRezultat) {
      izrisRezultatov(latlng, true);  
    });
  }

  mapa.on('click', obKlikuNaMapo);
  console.log("Klicem funkcijo");
  
  dodajFakultete();
});

function vrniIndeks(znacilnosti, string1, string2, callback) {
  var indeks = 0;
  var minRazdalija = Number.MAX_SAFE_INTEGER;
  console.log(znacilnosti[0]);
  callback(indeks);
}

function izrisiKlik(latlng) {
  console.log("Izrisi klik");
  
  pridobiPodatke(function(jsonRezultat) {
    
    var znacilnosti = jsonRezultat.features;
    
    for (var i = 0; i < rezultati.length; i++) {
      //console.log(rezultati[i]);
      mapa.removeLayer(rezultati[i]); 
    }
    
    for (var i = 0; i < znacilnosti.length; i++) {
      var prva1 = latlng.lat;
      var druga1 = latlng.lng;
      var prva2;
      var druga2;
      
      
      if (znacilnosti[i].geometry.type === "Polygon") {
        prva2 = znacilnosti[i].geometry.coordinates[0][0][1];
        druga2 = znacilnosti[i].geometry.coordinates[0][0][0];
      } else if (znacilnosti[i].geometry.type === "LineString") {
        prva2 = znacilnosti[i].geometry.coordinates[0][0][1];
        druga2 = znacilnosti[i].geometry.coordinates[0][0][0];
      }
      
      
      var latlng2 = L.latLng(prva2, druga2);
      var razdalija = distance(prva1, druga1, prva2, druga2);
      var zacasna = znacilnosti[i].geometry.coordinates;
      
      for (var j = 0; j < zacasna[0].length; j++) {
        var zacasna1 = zacasna[0][j][0];
        var zacasna2 = zacasna[0][j][1];
          
        zacasna[0][j][1] = zacasna1;
        zacasna[0][j][0] = zacasna2;
      }
        
      if (zacasna.length > 1) {
        for (var j = 0; j < zacasna[1].length; j++) {
          zacasna1 = zacasna[1][j][0];
          zacasna2 = zacasna[1][j][1];
            
          zacasna[1][j][0] = zacasna2;
          zacasna[1][j][1] = zacasna1;
        }
      }
      
      if (razdalija < 2) {
        
        var poligon = zacasna;
        var polygon = L.polygon(poligon, {color: 'green'}).addTo(mapa);
        rezultati.push(polygon);
        
        var name = znacilnosti[i].properties.name;
        var addr = znacilnosti[i].properties['addr:street'];
        console.log("Popup");
        polygon.bindPopup((name + '<br>') + (addr));
        
      } else if (znacilnosti[i].geometry.type === "Polygon" || 
                 znacilnosti[i].geometry.type === "LineString") {
        var poligon = zacasna;
        var polygon = L.polygon(poligon, {color: 'blue'}).addTo(mapa);
        rezultati.push(polygon);
      }
    }
  });
}
/**
 * Na zemljevid dodaj oznake z bližnjimi fakultetami in
 * gumb onemogoči.
 */
function dodajFakultete() {
  console.log("zacetek funkcije");
  pridobiPodatke(function (jsonRezultat) {
    izrisRezultatov(null, false);
    console.log("Dodaj fakultete funkcija dela");
  });
}

/**
 * Za podano vrsto interesne točke dostopaj do JSON datoteke
 * in vsebino JSON datoteke vrni v povratnem klicu
 * 
 * @param vrstaInteresneTocke "fakultete" ali "restavracije"
 * @param callback povratni klic z vsebino zahtevane JSON datoteke
 */
function pridobiPodatke(callback) {

  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        var json = JSON.parse(xobj.responseText);
        
        // nastavimo ustrezna polja (število najdenih zadetkov)
        
        // vrnemo rezultat
        callback(json);
    }
  };
  xobj.send(null);
}




/**
 * Dodaj izbrano oznako na zemljevid na določenih GPS koordinatah,
 * z dodatnim opisom, ki se prikaže v oblačku ob kliku in barvo
 * ikone, glede na tip oznake (FRI = rdeča, druge fakultete = modra in
 * restavracije = zelena)
 * 
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 * @param opis sporočilo, ki se prikaže v oblačku
 * @param tip "FRI", "restaurant" ali "faculty"
 */
function dodajMarker(lat, lng, opis, tip) {
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-icon-2x-' + 
      (tip == 'FRI' ? 'red' : (tip == 'restaurant' ? 'green' : 'blue')) + 
      '.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  // Ustvarimo marker z vhodnima podatkoma koordinat 
  // in barvo ikone, glede na tip
  var marker = L.marker([lat, lng], {icon: ikona});

  // Izpišemo želeno sporočilo v oblaček
  marker.bindPopup("<div>Naziv: " + opis + "</div>").openPopup();

  // Dodamo točko na mapo in v seznam
  marker.addTo(mapa);
  markerji.push(marker);
}


/**
 * Prikaz poti od/do izbrane lokacije do/od FRI
 * 
 * @param latLng izbrana točka na zemljevidu
 */
function prikazPoti(latLng) {
  // Izbrišemo obstoječo pot, če ta obstaja
  if (pot != null) mapa.removeControl(pot);

  // pot = ...
}


/**
 * Preveri ali izbrano oznako na podanih GPS koordinatah izrišemo
 * na zemljevid glede uporabniško določeno vrednost radij, ki
 * predstavlja razdaljo od FRI.
 * 
 * Če radij ni določen, je enak 0 oz. je večji od razdalje izbrane
 * oznake od FRI, potem oznako izrišemo, sicer ne.
 * 
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 */
function prikaziOznako(lng, lat) {
  var radij = 0;
  if (radij == 0)
    return true;
  else if (distance(lat, lng, FRI_LAT, FRI_LNG, "K") >= radij) 
    return false;
  else
    return true;
}

function izrisRezultatov(latlng, zacetek) {
  pridobiPodatke(function(jsonRezultat) {
    var znacilnosti = jsonRezultat.features;
    for (var i = 0; i < rezultati.length; i++) {
      mapa.removeLayer(rezultati[i]);
    }
    
    for (var i = 0; i < znacilnosti.length; i++) {
      // ce je oblike poligon 
      if (znacilnosti[i].geometry.type === "Polygon") {
        console.log("Je poligon");
        // obrnemo koordinate
        for (var j = 0; j < znacilnosti[i].geometry.coordinates[0].length; j++) {
          var first = znacilnosti[i].geometry.coordinates[0][j][0];
          var second = znacilnosti[i].geometry.coordinates[0][j][1];
          
          znacilnosti[i].geometry.coordinates[0][j][0] = second;
          znacilnosti[i].geometry.coordinates[0][j][1] = first;
        }
        if (znacilnosti[i].geometry.coordinates.length > 1) {
          for (var j = 0; j < znacilnosti[i].geometry.coordinates[1].length; j++) {
            var first = znacilnosti[i].geometry.coordinates[1][j][0];
            var second = znacilnosti[i].geometry.coordinates[1][j][1];
            
            znacilnosti[i].geometry.coordinates[1][j][0] = second;
            znacilnosti[i].geometry.coordinates[1][j][1] = first;
          }
        }  
        
        if (zacetek) {
          var razdalija = distance(latlng.lat, latlng.lng, znacilnosti[i].geometry.coordinates[0][0][0], znacilnosti[i].geometry.coordinates[0][0][1]);
          
          if (razdalija < 2) {
            var poligon = znacilnosti[i].geometry.coordinates;
            var polygon = L.polygon(poligon, {color: 'green'}).addTo(mapa);
            rezultati.push(polygon);
            var name = znacilnosti[i].properties.name;
            var addr = znacilnosti[i].properties['addr:street'];
            polygon.bindPopup((name + '<br>') + addr);
          }
        } else {
            var poligon = znacilnosti[i].geometry.coordinates;
            var polygon = L.polygon(poligon, {color: 'blue'}).addTo(mapa);  
            rezultati.push(polygon);
        }
        
        
      } else if (znacilnosti[i].geometry.type === "LineString") {
        console.log("Je LineString");
        for (var j = 0; j < znacilnosti[i].geometry.coordinates[0].length; j++) {
          var first = znacilnosti[i].geometry.coordinates[0][j][0];
          var second = znacilnosti[i].geometry.coordinates[0][j][1];
          
          znacilnosti[i].geometry.coordinates[0][j][0] = second;
          znacilnosti[i].geometry.coordinates[0][j][1] = first;
        }
        // izrisemo LineString kot poligon
        if (zacetek) {
          var razdalija = distance(latlng.lat, latlng.lng, znacilnosti[i].geometry.coordinates[0][0][0], znacilnosti[i].geometry.coordinates[0][0][1]);
          if (razdalija < 2) {
            var poligon = znacilnosti[i].geometry.coordinates;
            var polygon = L.polygon(poligon, {color: 'green'}).addTo(mapa);
            rezultati.push(polygon);          
          } 
        } else {
            var poligon = znacilnosti[i].geometry.coordinates;
            var polygon = L.polygon(poligon, {color: 'blue'}).addTo(mapa);
            rezultati.push(polygon);        
        }
      }
    }
  });  
}