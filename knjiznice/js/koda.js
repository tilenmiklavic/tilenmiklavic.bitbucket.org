
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";
var generiraniEHR = [""];


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta, callback) {
  console.log("i"+stPacienta);
  var ehrId_return;
  var ime, priimek, datumRojstva;
  console.log("Kreiraj EHR za bolnika");
  if (stPacienta == 1) {
    ime = 'Lojze';
    priimek = 'Novak';
    datumRojstva = '1950-03-15';
  } else if (stPacienta == 2) {
    ime = 'Marjeta';
    priimek = 'Majcen';
    datumRojstva = '2003-10-20';
  } else if (stPacienta == 3) {
    ime = 'Adolf';
    priimek = 'Kocjan';
    datumRojstva = '1989-04-20';
  }

		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        generiraniEHR[0] = ehrId;
        for (var j = 1; j < 6; j++) {
          shranjevanjeNoveSerijePodatkov2(ehrId, j, stPacienta);
        }
        callback(ehrId);
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#preberiEHRid").val(ehrId);
              if (ime != undefined) {
                $("#form_select_generated").append("<option value="+ehrId+">"+ime+" "+priimek+"</option>");
                console.log(ime+priimek);
              }  
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
}

function kreirajEHRzaBolnika() {
  console.log("Kreiraj EHR za bolnika");
	var ime = $("#form-ime").val();
	var priimek = $("#form-priimek").val();
  var datumRojstva = $("#form-datum").val();
  console.log(datumRojstva.toString());

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              $("#preberiEHRid").val(ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
	}
} 

function poizvedbaPoImenu() {
  $.ajaxSetup({
    headers: {
        "Authorization": authorization
    }
  });
  var searchData = [
    {key: "firstNames", value: "Mary"},
    {key: "lastNames", value: "Wilkinson"}
  ];
  $.ajax({
    url: baseUrl + "/demographics/party/query",
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(searchData),
    success: function (res) {
        $("#header").html("Search for Mary Wilkinson");
        for (var i in res.parties) {
          var party = res.parties[i];
          var ehrId;
          for (var j in party.partyAdditionalInfo) {
            if (party.partyAdditionalInfo[j].key === 'ehrId') {
              ehrId = party.partyAdditionalInfo[j].value;
              break;
            }
          }
            $("#result").append(party.firstNames + ' ' + party.lastNames +
            ' (ehrId = ' + ehrId + ')<br>');
           
        }
    }
  });
}

function poizvedbaPoEHRstevilki() {
  if ($("#form-ime_poizvedba").val() != '') {
    iskanjeImenaZehrStevilko();
    return;
  } else if ($("#form-select").val() != 'znak') {
    iskanjeSpecificnegaVitalnegaZnaka();
    return;
  }
  console.log("Poizvedba po EHR stevilki");
  $.ajaxSetup({
    headers: {
      "Authorization": getAuthorization()
    }
  });
  var stevilka = $("#form-ehr").val()
  var searchData = [
    {key: "ehrId", value: stevilka}
  ];
  $.ajax({
    url: baseUrl + "/demographics/party/query",
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(searchData),
    success: function (res) {
      $("#header").html("Search by ehrId" + stevilka);
      if (res == undefined) {
        console.log("Undefined");
        $("#kreirajSporocilo2").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka</span>");
      } else {
        for (var i in res.parties) {
          var party = res.parties[i];
          $("#kreirajSporocilo2").html("<span class='obvestilo label label-success fade-in'>" +
             party.firstNames + ' ' + party.lastNames + '<br>');
        }
      }  
    },
  });
}

function shranjevanjeNoveSerijePodatkov() {
  var datum = new Date();
  var month = datum.getMonth();
  if (month < 10) {
    month = '0' + month;
  }
  var dan = datum.getDay();
  if (dan < 10) {
    dan = '0' + dan;
  }
  var ura = datum.getHours();
  if (ura < 10) {
    ura = '0' + ura;
  }
  var minute = datum.getMinutes();
  if (minute < 10) {
    minute = '0' + minute;
  }
  var sekunde = datum.getSeconds();
  if (sekunde < 10) {
    sekunde = '0' + sekunde;
  }
	var ehrId = $("#form-ehrstevilka").val();
	var datumInUra = datum.getFullYear()+"-"+month+"-"+dan+'T'+ura+":"+minute+":"+sekunde+"Z";
	console.log(datumInUra);
	var telesnaVisina = $("#form-visina").val();
	var telesnaTeza = $("#form-teza").val();
	var telesnaTemperatura = $("#form-temperatura").val();
	var sistolicniKrvniTlak = $("#form-sistolicni").val();
	var diastolicniKrvniTlak = $("#form-diasistolicni").val();
	var nasicenostKrviSKisikom = '';
	var merilec = '';

	if (!ehrId || ehrId.trim().length == 0) {
		$("#Sporocilo3").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        $("#Sporocilo3").html(
          "<span class='obvestilo label label-success fade-in'>" +
          "Meritev uspesno dodana" + ".</span>");
      },
      error: function(err) {
      	$("#Sporocilo3").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}
}

function shranjevanjeNoveSerijePodatkov2(ehrId, j, stPacienta) {
  console.log("i" + stPacienta);
  
  var month, dan, ura, minute, sekunde, telesnaVisina, telesnaTeza, telesnaTemperatura, sistolicniKrvniTlak, diastolicniKrvniTlak, nasicenostKrviSKisikom;
  if (stPacienta == 1) {
    console.log("prvi pacient");
    j++;
    month = '0' + j;
    dan = 10 + j;
    ura = 13 + j;
    minute = 30 + j;
    sekunde = 40 + j;
    
    telesnaVisina = 150 + j;
    telesnaTeza = 80 + 3*j;
    telesnaTemperatura = 27;
    sistolicniKrvniTlak = 120;
    diastolicniKrvniTlak = 60;
    nasicenostKrviSKisikom = 70;
    
  }  else if (stPacienta == 2) {
    j++;
    month = '0' + j;
    dan = 10 + j;
    ura = 13 + j;
    minute = 30 + j;
    sekunde = 40 + j;
    
    telesnaVisina = 210 + 2*j;
    telesnaTeza = 90 + j;
    telesnaTemperatura = 26;
    sistolicniKrvniTlak = 140;
    diastolicniKrvniTlak = 80;
    nasicenostKrviSKisikom = 60;
    
  } else if (stPacienta == 3) {
    j++;
    month = '0' + j;
    dan = 10 + j;
    ura = 13 + j;
    minute = 30 + j;
    sekunde = 40 + j;
    
    telesnaVisina = 180 + j;
    telesnaTeza = 50 + 4*j;
    telesnaTemperatura = 26,5;
    sistolicniKrvniTlak = 130;
    diastolicniKrvniTlak = 30;
    nasicenostKrviSKisikom = 40;
    
  }
  
  var datum = new Date();

	var datumInUra = datum.getFullYear()+"-"+month+"-"+dan+'T'+ura+":"+minute+":"+sekunde+"Z";
	console.log(datumInUra);
	var merilec = '';

	if (!ehrId || ehrId.trim().length == 0) {
		$("#Sporocilo3").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		console.log(ehrId);
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        $("#Sporocilo3").html(
          "<span class='obvestilo label label-success fade-in'>" +
          "Meritev uspesno dodana" + ".</span>");
      },
      error: function(err) {
      	$("#Sporocilo3").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}
}

function iskanjeSpecificnegaVitalnegaZnaka() {
  $.ajax({
    url: baseUrl + "/demographics/ehr/" + $("#form-ehr").val() + "/party",
    type: 'GET',
    headers: {
        "Authorization": getAuthorization()
    },
    success: function (data) {
        var party = data.party;
        $("#header").append("Weight measurements for " + party.firstNames + ' ' +
                                                         party.lastNames);
    }
  });
  var izbira = $("#form-select").val();

  
  if (izbira == 'weight') {
    $.ajax({
      url: baseUrl + "/view/" + $("#form-ehr").val() + "/weight",
      type: 'GET',
      headers: {
          "Authorization": getAuthorization()
      },
      success: function (res) {
        izrisiGraf(res);
        for (var i in res) {
          $("#kreirajSporocilo2").append(res[i].time + ': ' + res[i].weight + res[i].unit + "<br>");
        }
      }
    });
  } else if (izbira == 'height') {
    $.ajax({
      url: baseUrl + "/view/" + $("#form-ehr").val() + "/height",
      type: 'GET',
      headers: {
          "Authorization": getAuthorization()
      },
      success: function (res) {
        console.log(res);
        izrisiGraf2(res);
        for (var i in res) {
          $("#kreirajSporocilo2").append(res[i].time + ': ' + res[i].height + res[i].unit + "<br>");
        }
      }
    });
  }  
}

function iskanjeImenaZehrStevilko() {
  console.log("Iskanje imena z ehr stevilko");
  $.ajaxSetup({
    headers: {
      "Authorization": getAuthorization()
    }
  });
  var fullName = $("#form-ime_poizvedba").val();
  var split = fullName.split(' ');
  var searchData = [
    {key: "firstNames", value: split[0]},
    {key: "lastNames", value: split[1]}
  ];
  $.ajax({
    url: baseUrl + "/demographics/party/query",
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(searchData),
    success: function (res) {
      $("#header").html("Search for requested person");
      for (var i in res.parties) {
        var party = res.parties[i];
        var ehrId;
        for (var j in party.partyAdditionalInfo) {
          if (party.partyAdditionalInfo[j].key === 'ehrId') {
            ehrId = party.partyAdditionalInfo[j].value;
            break;
          }
        }
        $("#kreirajSporocilo2").append(party.firstNames + ' ' + party.lastNames +
        ' (ehrId = ' + ehrId + ')<br>');
        console.log(ehrId);
      }
    }
  });
}

function izrisiGraf(res) {
  console.log("Izrisi graf");
  
  
  var data = [];
  for (var i = res.length - 1; i >= 0; i--) {
    var date = res[i].time;
    date = date.substring(0,4);
    data.push({
      x: i + 1,
      y: res[i].weight
    });
    console.log(date);
  }  
  console.log(data);
  
  var chart = new CanvasJS.Chart("graf", {
    title:{
      text: "Prikaz teze"
    },
    axisY:{
      includeZero: false
    },
    data: [{
      type: 'line',
      dataPoints: data
    }]
  });
  chart.render();
}

function izrisiGraf2(res) {
  console.log("Izrisi graf");
  
  var data = [];
  for (var i = res.length - 1; i >= 0; i--) {
    //console.log(res[i].height);
    //var date = res[i].time;
    //date = date.substring(0,4);
    data.push({
      x: i + 1,
      y: res[i].height
    });
  }  
  console.log(data);
  
  var chart = new CanvasJS.Chart("graf", {
    title:{
      text: "Prikaz visine"
    },
    axisY:{
      includeZero: false
    },
    data: [{
      type: 'line',
      dataPoints: data
    }]
  });
  chart.render();
}

function generiranje() {
  console.log("Generiranje");
  
  for (var i = 1; i < 4; i++) {
    generirajPodatke(i, function(ehr) {
    });  
  }
}  

function izpolnjevanjePodatkov() {
  console.log("Izpolnjevanje podatkov");
  var ehrId = document.getElementById("form_select_generated").value;
  document.getElementById("form-ehrstevilka").value = ehrId;
  document.getElementById("form-ehr").value = ehrId;
}

function vreme(indeks) {
  if (indeks == 1) {
    $("#tweet").append('<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">These Johns Hopkins students are slashing breast cancer biopsy costs <a href="https://t.co/Q6NQxz6w7p">https://t.co/Q6NQxz6w7p</a> <a href="https://t.co/YuklNSFc1Z">pic.twitter.com/YuklNSFc1Z</a></p>&mdash; Eugen Platon (@eugenplaton) <a href="https://twitter.com/eugenplaton/status/1130253313935437826?ref_src=twsrc%5Etfw">May 19, 2019</a></blockquote><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>')
  } else if (indeks == 2) {
    $("#tweet").append('<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Cultivating cells in a Petri dish is a time-honoured way of experimenting on biological tissues. But it is not particularly reliable  <a href="https://t.co/LL1LPde0GV">https://t.co/LL1LPde0GV</a></p>&mdash; The Economist (@TheEconomist) <a href="https://twitter.com/TheEconomist/status/1129790819991470081?ref_src=twsrc%5Etfw">May 18, 2019</a></blockquote><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>')
  } else if (indeks == 3) {
    $("#tweet").append('<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Regular physical exercise, not using tobacco, drinking less alcohol, maintaining healthy blood pressure and eating a healthy diet are all linked to reducing the risk of dementia, according to the World Health Organization <a href="https://t.co/pGrY3iZDtz">https://t.co/pGrY3iZDtz</a></p>&mdash; CNN (@CNN) <a href="https://twitter.com/CNN/status/1130012733099991040?ref_src=twsrc%5Etfw">May 19, 2019</a></blockquote><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>')

  } else if (indeks == 4) {
    $("#tweet").append('<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Scientists develop jab that stops craving for junk food <a href="https://t.co/ckgHRQMSVG">https://t.co/ckgHRQMSVG</a></p>&mdash; The Sun (@TheSun) <a href="https://twitter.com/TheSun/status/1128352552532226049?ref_src=twsrc%5Etfw">May 14, 2019</a></blockquote><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>')
    
  } else if (indeks == 5) {
    $("#tweet").append('<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">First piece for the <a href="https://twitter.com/SCMP_Sport?ref_src=twsrc%5Etfw">@SCMP_Sport</a> &#39;The words “always the victims, it’s never your fault” are directed at Liverpool fans. Everyone denies it is a Hillsborough slur but it&#39;s the clearest dog whistle in the history of high-pitched whines.&#39;<a href="https://t.co/ejWEIU6LZk">https://t.co/ejWEIU6LZk</a></p>&mdash; Tony Evans (@TonyEvans92a) <a href="https://twitter.com/TonyEvans92a/status/1129350663895437312?ref_src=twsrc%5Etfw">May 17, 2019</a></blockquote><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>')
    
  } else if (indeks == 6) {
    $("#tweet").append('<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">BBC Sport - Man City win treble - how impressive is that achievement? <a href="https://t.co/Zeba0n7QEv">https://t.co/Zeba0n7QEv</a>. Very impressive and all done with academy players!  I think not.  Not one academy player.</p>&mdash; James Isherwood (@JamesIsherwoo15) <a href="https://twitter.com/JamesIsherwoo15/status/1130022379437154304?ref_src=twsrc%5Etfw">May 19, 2019</a></blockquote><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>')
    
  }
}

function primorska() {
  console.log("Primorska");
  $("#primorska").append("<ul>" +
                         "<li><a onclick='vreme(1)'>Rak</a></li>" + 
                         "<li><a onclick='vreme(2)'>Medicinske raziskave</a></li>" + 
                         "</ul>");
}

function gorenjska() {
  console.log("Gorenjska");
  $("#gorenjska").append("<ul>" +
                         "<li><a onclick='vreme(3)'>Diete</a></li>" + 
                         "<li><a onclick='vreme(4)'>Slaba prehrana</a></li>" + 
                         "</ul>");
}

function notranjska() {
  console.log("Notranjska");
  $("#notranjska").append("<ul>" +
                         "<li><a onclick='vreme(5)'>Sport</a></li>" + 
                         "<li><a onclick='vreme(6)'>Sportni dosezki</a></li>" + 
                         "</ul>");
}


$(document).ready(function () {
  //your code here
  console.log("Aplikacija pripravljena");
});

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
